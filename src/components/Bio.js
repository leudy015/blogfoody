import React from "react";
import styles from "./Bio.module.css";
import { getGravatarURL } from "../utils/getGravatarURL";
import { FaTwitter, FaUser } from "react-icons/fa";

function Bio(props) {
  let photoURL = getGravatarURL({
    email: props.data.email,
    size: 40,
  });

  return (
    <div
      className={`
      ${styles.Bio}
      ${props.className || ""}
    `}
    >
      <img src={photoURL} alt={props.data.name} />
      <div>
        <a href={props.data.twitter}>
          <FaTwitter /> {props.data.name}
        </a>
        <p className={styles.desc}>
          <FaUser /> {props.data.puesto}
        </p>
      </div>
    </div>
  );
}

export default Bio;
