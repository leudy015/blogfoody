export default {
  getImg: () => import("./post.jpg"),
  title: `Los 8 beneficios de los pedidos en línea para restaurantes`,
  tags: ["Beneficios", "Pedidos Online", "Tips", "Restaurante"],
  spoiler:
    "La industria de los restaurantes ha cambiado a un ritmo acelerado en la última década. Con una oleada de clientes que se mueven hacia los pedidos en línea, se ha vuelto vital para los restaurantes expandir sus servicios más allá de las comidas tradicionales y para llevar. Un sitio web y un menú en línea",
  bio: {
    name: "Leudy Martes",
    puesto: "CEO Foody®",
    twitter: "https://twitter.com/leudymartes",
    email: "leudy1521@gmail.com",
  },
  img: "https://foody-img.s3.eu-west-3.amazonaws.com/posthoy.jpg",
  getContent: () => import("./document.mdx"),
};
