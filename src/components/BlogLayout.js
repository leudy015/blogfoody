import React from "react";
import { View, NotFoundBoundary, useLoadingRoute, Link } from "react-navi";
import NotFoundPage from "./NotFoundPage";
import LoadingIndicator from "./LoadingIndicator";
import styles from "./BlogLayout.module.css";
import mainLogo from "./../assets/img/logo.svg";

function BlogLayout() {
  const loadingRoute = useLoadingRoute();

  return (
    <>
      <nav>
        <div className={styles.navbarLogo}>
          <Link href="/">
            <img src={mainLogo} width="150px" height="auto" alt="logo" />
          </Link>
        </div>
      </nav>

      <div>
        <LoadingIndicator active={!!loadingRoute} />

        <main>
          <NotFoundBoundary render={() => <NotFoundPage />}>
            <View />
          </NotFoundBoundary>
        </main>
      </div>
    </>
  );
}

export default BlogLayout;
