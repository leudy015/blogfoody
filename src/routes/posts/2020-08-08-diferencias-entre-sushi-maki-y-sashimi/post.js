export default {
  getImg: () => import("./post.jpg"),
  title: `Diferencias entre sushi, maki y sashimi`,
  tags: ["Sushi", "maki", "Tips", "sashimi"],
  spoiler:
    "Sushi, maki, sashimi, nigri… A todos nos ha pasado, las primeras veces que vamos a un restaurante japonés nos sentimos un poco abrumados ante la cantidad de nombres que reciben los platos que habitualmente conocemos bajo el nombre de sushi. Y muchas veces,",
  bio: {
    name: "Leudy Martes",
    puesto: "CEO Foody®",
    twitter: "https://twitter.com/leudymartes",
    email: "leudy1521@gmail.com",
  },
  img: "https://blogfoody.s3.eu-west-3.amazonaws.com/sushi.jpg",
  getContent: () => import("./document.mdx"),
};
