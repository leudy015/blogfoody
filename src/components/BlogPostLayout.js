import React from "react";
import { Link, useCurrentRoute, useView } from "react-navi";
import { MDXProvider } from "@mdx-js/react";
import ArticleMeta from "./ArticleMeta";
import Bio from "./Bio";
import ShareButtons from "./share";
import styles from "./BlogPostLayout.module.css";

function BlogPostLayout({ blogRoot }) {
  let { title, data } = useCurrentRoute();
  let { connect, content, head } = useView();
  let { MDXComponent, readingTime } = content;
  // The content for posts is an MDX component, so we'll need
  // to use <MDXProvider> to ensure that links are rendered
  // with <Link>, and thus use pushState.
  return connect(
    <>
      {head}
      <article>
        <div className={styles.headerContent}>
          <div className={styles.header}>
            <h1>{title}</h1>
            <div className={styles.InfoShare}>
              <div className={styles.BioMeta}>
                <Bio className={styles.Bio} data={data.bio} />
                <ArticleMeta
                  blogRoot={blogRoot}
                  meta={data}
                  readingTime={readingTime}
                />
              </div>
              <ShareButtons />
            </div>
          </div>
        </div>

        <MDXProvider
          components={{
            a: Link,
            wrapper: ({ children }) => (
              <div className={styles.content}>{children}</div>
            ),
          }}
        >
          <MDXComponent />
        </MDXProvider>

        <footer className={styles.footer}>
          <div className={styles.related}>
            <h2>Continua leyendo</h2>

            <div className={styles.CardsContainer}>
              <div className={styles.relatedCard}>
                {data.previousDetails && (
                  <Link href={data.previousDetails.href}>
                    ← {data.previousDetails.title}
                  </Link>
                )}
              </div>

              <div className={styles.relatedCard}>
                {data.nextDetails && (
                  <Link href={data.nextDetails.href}>
                    {data.nextDetails.title} →
                  </Link>
                )}
              </div>
            </div>

            <div
              className="fb-comments"
              data-href="https://developers.facebook.com/docs/plugins/comments#configurator"
              data-width=""
              data-numposts="10"
            ></div>
          </div>
        </footer>
      </article>
    </>
  );
}

export default BlogPostLayout;
