export default {
  getImg: () => import("./post.jpg"),
  title: `Digitaliza tu restaurante`,
  tags: ["Marketing", "Restaurante", "Tips", "Cocina"],
  spoiler:
    "El uso de la tecnología en el sector de la restauración y la hostelería es una oportunidad para adquirir presencia y crecer, pero sobre todo para gestionar de una manera más eficaz tu restaurante. El mundo es digital, tus clientes son digitales… ¿es tu restaurante digital?",
  bio: {
    name: "Leudy Martes",
    puesto: "CEO Foody®",
    twitter: "https://twitter.com/leudymartes",
    email: "leudy1521@gmail.com",
  },
  img: "https://foody-img.s3.eu-west-3.amazonaws.com/imagen2.jpg",
  getContent: () => import("./document.mdx"),
};
