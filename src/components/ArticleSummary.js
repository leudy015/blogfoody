import React from "react";
import { Link } from "react-navi";
import ArticleMeta from "./ArticleMeta";
import styles from "./ArticleSummary.module.css";
import Bio from "./Bio";

function ArticleSummary({ blogRoot, route, bio, img }) {
  return (
    <Link href={route.url.href}>
      <div className={styles.card}>
        <img src={img} className={styles.PosIMG} alt={route.title} />
        <article className={styles.ArticleSummary}>
          <h2>
            <Link href={route.url.href}>{route.title}</Link>
          </h2>
          <ArticleMeta blogRoot={blogRoot} meta={route.data} />
          <p>{route.data.spoiler}</p>
        </article>
        <Bio data={bio} />
      </div>
    </Link>
  );
}

export default ArticleSummary;
