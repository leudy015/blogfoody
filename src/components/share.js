import React from "react";
import { useCurrentRoute } from "react-navi";
import styles from "./share.module.css";

import {
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton,
} from "react-share";

import {
  FacebookIcon,
  TwitterIcon,
  WhatsappIcon,
  LinkedinIcon,
} from "react-share";

function ShareButtons() {
  let { url } = useCurrentRoute();

  return (
    <div className={styles.ShareButtons}>
      <FacebookShareButton url={"https://blog.foodyapp.es" + url.pathname}>
        <FacebookIcon size={40} round={true} />
      </FacebookShareButton>

      <TwitterShareButton url={"https://blog.foodyapp.es" + url.pathname}>
        <TwitterIcon size={40} round={true} />
      </TwitterShareButton>

      <LinkedinShareButton url={"https://blog.foodyapp.es" + url.pathname}>
        <LinkedinIcon size={40} round={true} />
      </LinkedinShareButton>

      <WhatsappShareButton url={"https://blog.foodyapp.es" + url.pathname}>
        <WhatsappIcon size={40} round={true} />
      </WhatsappShareButton>
    </div>
  );
}

export default ShareButtons;
