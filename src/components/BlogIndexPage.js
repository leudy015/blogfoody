import React from "react";
import ArticleSummary from "./ArticleSummary";
import Pagination from "./Pagination";
import styles from "./BlogIndexPage.module.css";
import imagen1 from "./../assets/img/imagen1.jpg";
import imagen2 from "./../assets/img/imagen2.jpg";
import imagen3 from "./../assets/img/imagen3.jpg";
import { Link } from "react-navi";
import CookieConsent from "react-cookie-consent";

var TxtType = function (el, toRotate, period) {
  this.toRotate = toRotate;
  this.el = el;
  this.loopNum = 0;
  this.period = parseInt(period, 10) || 2000;
  this.txt = "";
  this.tick();
  this.isDeleting = false;
};

TxtType.prototype.tick = function () {
  var i = this.loopNum % this.toRotate.length;
  var fullTxt = this.toRotate[i];

  if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
  } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
  }

  this.el.innerHTML = '<span class="wrap">' + this.txt + "</span>";

  var that = this;
  var delta = 200 - Math.random() * 100;

  if (this.isDeleting) {
    delta /= 2;
  }

  if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
  } else if (this.isDeleting && this.txt === "") {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
  }

  setTimeout(function () {
    that.tick();
  }, delta);
};

window.onload = function () {
  var elements = document.getElementsByClassName("typewrite");
  for (var i = 0; i < elements.length; i++) {
    var toRotate = elements[i].getAttribute("data-type");
    var period = elements[i].getAttribute("data-period");
    if (toRotate) {
      new TxtType(elements[i], JSON.parse(toRotate), period);
    }
  }
  // INJECT CSS
  var css = document.createElement("style");
  css.type = "text/css";
  css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #ffcb40}";
  document.body.appendChild(css);
};

function BlogIndexPage({ blogRoot, pageCount, pageNumber, postRoutes }) {
  return (
    <div className={styles.indexContainer}>
      <section className={styles.indexHeader}>
        <div className={styles.blogTitle}>
          <h1
            className="typewrite"
            data-period="2000"
            data-type='[ "Bienvenidos al blog de Foody", "entérate de todas las noticias", "de nuestra comunidad"]'
          >
            <span className="wrap"></span>
          </h1>
          <p>
            Somos el equipo de prensa, contenido, investigación y diseño de
            Foody® Pick Up. Ayudamos a digitalizar los restaurantes para que de
            está manera puedan llegar a más clientes con nuestra potente
            herramienta.
          </p>
          <Link href="https://foodyapp.es/restaurant">
            ¿Tienes un restaurante?
          </Link>
          <Link href="https://foodyapp.es">Buscar restaurante</Link>
        </div>

        <div className={styles.wave}></div>
        <div className={styles.wave2}></div>
        <div className={styles.wave3}></div>
        <div className={styles.wave4}></div>
      </section>

      <div className={styles.Features}>
        <div className={styles.FeatureContainer}>
          <img src={imagen1} alt="image1" />

          <div className={styles.FeatureText}>
            <h3>Asociación con 10.000 restaurantes</h3>
            <p>
              En el equipo de restaurantes, estamos ayudando activamente a
              remodelar la industria de los restaurantes. Estamos creando las
              herramientas que optimizan las operaciones diarias de los
              restaurantes y les brindan formas de comprender y llegar a nuevos
              clientes.
            </p>
          </div>
        </div>

        <div className={styles.FeatureContainer}>
          <img src={imagen3} alt="image1" />

          <div className={styles.FeatureText}>
            <h3>Automatizamos tu marketing digital</h3>
            <p>
              Ponemos todos nuestros recursos y medios a tu servicio para
              ayudarte a reducir costes e impulsar tu negocio, por ellos nos
              enfocamos en los medio digitales para llevar los restaurantes del
              barrio a los móviles de tus futuros clientes.
            </p>
          </div>
        </div>

        <div className={styles.FeatureContainer}>
          <img src={imagen2} alt="image1" />

          <div className={styles.FeatureText}>
            <h3>Digitalizamos tu negocio</h3>
            <p>
              Sin duda en los últimos tiempos es de gran importacia tener
              presencia en el mundo online ya que en estos canales podemos
              inpulsar nuestro negocio a otro nivel, por ello en Foody estamos
              enfocado en desarollar la mejor herramienta, Que espera para
              unirte a esta revolución.
            </p>
          </div>
        </div>
      </div>

      <div className={styles.blogPosts}>
        <div className={styles.blogPostsHeader}>
          <h1>Lee nuestro blog</h1>
          <p>
            Cosas en las que estamos trabajando, eventos a los que vamos,
            productos que usamos y personas que conocemos.
          </p>
        </div>

        <ul className={styles.articlesList}>
          {postRoutes.map((route) => (
            <li key={route.url.href}>
              <ArticleSummary
                blogRoot={blogRoot}
                route={route}
                bio={route.data.bio}
                img={route.data.img}
              />
            </li>
          ))}
        </ul>
        {pageCount > 1 && (
          <Pagination
            blogRoot={blogRoot}
            pageCount={pageCount}
            pageNumber={pageNumber}
          />
        )}
      </div>
      <CookieConsent
        location="bottom"
        buttonText="Aceptar"
        cookieName="myAwesomeCookieName2"
        style={{
          background: "#eeeeee",
          color: "black",
        }}
        buttonStyle={{
          color: "white",
          fontSize: "14px",
          backgroundColor: "#ffcb40",
          borderRadius: 5,
          width: 150,
          outline: "none",
        }}
        expires={150}
      >
        Este sitio web utiliza cookies para garantizar que obtenga la mejor
        experiencia en nuestro sitio web.{" "}
        <span style={{ fontSize: "10px" }}>
          Más detalles en https://foodyapp.es/cookies
        </span>
      </CookieConsent>
    </div>
  );
}

export default BlogIndexPage;
