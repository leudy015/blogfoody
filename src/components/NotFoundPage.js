import React from "react";
import { Link } from "react-navi";
import styles from "./NotFoundPage.module.css";
import Depresion from "./../assets/img/depresion.svg";
import { FaArrowLeft } from "react-icons/fa";

// Note that create-react-navi-app will always show an error screen when this
// is rendered. This is because the underlying react-scripts package shows
// the error screen when a NotFoundError is thrown, even though it's caught
// by <NotFoundBoundary>. To see the error rendered by this function,
// you'll just need to close the error overlay with the "x" at the top right.
function NotFoundPage() {
  return (
    <div className={styles.NotFound}>
      <section className={styles.hero}>
      <div className={styles.hero}>
        <h1>404 Pagina no encontrada</h1>
      </div>
      <div className={styles.wave}></div>
        <div className={styles.wave2}></div>
        <div className={styles.wave3}></div>
        <div className={styles.wave4}></div>
      </section>

      <div className={styles.depression}>
        <img src={Depresion} alt="depression" />
        <h3>Parece que no podemos encontrar lo que estás buscando.</h3>
        <Link className={styles.homeLink} href="/">
          <FaArrowLeft /> Volver al inicio
        </Link>
      </div>
    </div>
  );
}

export default NotFoundPage;
