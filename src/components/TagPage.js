import React from "react";
import ArticleSummary from "./ArticleSummary";
import styles from "./TagPage.module.css";

function TagPage({ blogRoot, name, routes }) {
  return (
    <div className={styles.TagPage}>
      <section className={styles.hero}>
        <div className={styles.hero}>
          <h1>{name} posts</h1>
        </div>

        <div className={styles.wave}></div>
        <div className={styles.wave2}></div>
        <div className={styles.wave3}></div>
        <div className={styles.wave4}></div>
      </section>

      <ul className={styles.articlesList}>
        {routes.map((route) => (
          <li key={route.url.href}>
            <ArticleSummary
              blogRoot={blogRoot}
              route={route}
              bio={route.data.bio}
              img={route.data.img}
            />
          </li>
        ))}
        {routes.map((route) => (
          <li key={route.url.href}>
            <ArticleSummary
              blogRoot={blogRoot}
              route={route}
              bio={route.data.bio}
              img={route.data.img}
            />
          </li>
        ))}
      </ul>
    </div>
  );
}

export default TagPage;
