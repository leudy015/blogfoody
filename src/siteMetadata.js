export default {
  // The blog's title as it appears in the layout header, and in the document
  // <title> tag.
  getImg: "https://foody-img.s3.eu-west-3.amazonaws.com/landing.png",
  title: "Blog Foody® Pick Up",
  author: "Foody",
  description:
    "Haz tu pedido con la app y recógelo en minutos en los restaurantes de tu ciudad.",

  // The number of posts to a page on the site index.
  indexPageSize: 20,
};
